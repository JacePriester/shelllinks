# README #

This code is free for use - no license, no warranty.

### What is this repository for? ###

* ShellLinks is a C# LNK file parser, and maybe more in the future (edit and save?)
* Allows you to read data directly from an LNK file without COM or other garbage.
* Resolve shortcut paths, find icon paths and index, etc.

* I'm completely certain that you're thinking "Hey, reading LNK files is not the proper way to resolve shortcuts. That's what Shell32.ShellLinkObject is for!". I have used that in the past and it resolves 99% of shortcuts fine, but some it doesn't. Perhaps there's a problem with using it from C# and it would work from C/C++ .. but the fact is it's quite buggy in C#. Having to add a reference to Shell32 and invoke a COM object also means that you can't call it from a background thread unless it has STAThread attribute, which rules out BackgroundWorkers. Between the threading limitations and poor performance of COM, it's just a bad solution and it's a shame we don't have a better one direct from Microsoft.

* The LNK file format is well documented (now, finally) and is not complicated. This solution allows you to read LNK files directly in C# and parse their contents. It is not a complete solution yet - some parts are not parsed - but the majority of the interesting stuff is there. This is not implemented from guesswork in reverse engineering the format - it is implemented as specified on MSDN and tested on hundreds of LNK files.

### How do I get set up? ###

* No dependencies, no references.
* Does not depend on Shell32 COM objects.
* Does not require WSH or any other nonsense.
* Call ShellLinks.ParseLnk(lnk_file_path_here) and examine the result. It is fairly self explanatory.
* For more detailed information see the MSDN data on the LNK file format:
* https://msdn.microsoft.com/en-us/library/dd891253.aspx

### Who do I talk to? ###

* Created by jace Priester
* jacepriester@gmail.com