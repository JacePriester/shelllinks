﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

public static partial class ShellLinks
{
    //https://msdn.microsoft.com/en-us/library/dd891345.aspx


    public enum DataBlockSignatures : UInt32
    {
        ConsoleDataBlock = 0xA0000002,
        ConsoleFEDataBlock = 0xA0000004,
        DarwinDataBlock = 0xA0000006,
        EnvironmentVariableDataBlock = 0xA0000001,
        IconEnvironmentDataBlock = 0xA0000007,
        KnownFolderDataBlock = 0xA000000B,
        PropertyStoreDataBlock = 0xA0000009,
        ShimDataBlock = 0xA0000008,
        SpecialFolderDataBlock = 0xA0000005,
        TrackerDataBlock = 0xA0000003,
        VistaAndAboveIDListDataBlock = 0xA000000C
    }

    //https://msdn.microsoft.com/en-us/library/dd871335.aspx
    public class ExtraData
    {
        public UInt32 BlockSize; //always >= 0xC
        public DataBlockSignatures BlockSignature;
        public byte[] RawData;
        public ExtraDataBlock Data;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public abstract class ExtraDataBlock
    {
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class ConsoleFEDataBlock : ExtraDataBlock            // A ConsoleFEDataBlock structure (section 2.5.2).
    {
        public UInt32 CodePage;         // unsigned integer that specifies a code page language
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class ConsoleDataBlock : ExtraDataBlock               // A ConsoleDataBlock structure (section 2.5.1).
    {
        public FillAttributes FillAttributes;
        public FillAttributes PopupFillAttributes;
        public UInt16 ScreenBufferSizeX, ScreenBufferSizeY;
        public UInt16 WindowSizeX, WindowSizeY;
        public UInt16 WindowOriginX, WindowOriginY;
        public UInt32 Unused1;
        public UInt32 Unused2;
        public UInt32 FontSize;
        public FontFamily FontFamily;
        public UInt32 FontWeight;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
        public byte[] FaceName;

        public UInt32 CursorSize;       // in pixels, <= 25 = small, 26-50 = medium, 51-100 large
        public UInt32 FullScreen;       // 0x0 = off
        public UInt32 QuickEdit;        // 0x0 = off
        public UInt32 InsertMode;       // 0x0 = disabled
        public UInt32 AutoPosition;     // 0x0 = WindowOriginX and WindowOriginY are used for positioning
        public UInt32 HistoryBufferSize;        // in characters
        public UInt32 NumberOfHistoryBuffers;
        public UInt32 HistoryNoDup;             // 0x0 = Duplicates are not allowed.

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public UInt32[] ColorTable;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class DarwinDataBlock : ExtraDataBlock                // A DarwinDataBlock structure (section 2.5.3).
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 260)]
        public byte[] DarwinDataAnsi;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 520)]
        public byte[] DarwinDataUnicode;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class EnvironmentVariableDataBlock : ExtraDataBlock           // An EnvironmentVariableDataBlock structure (section 2.5.4).
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 260)]
        public byte[] TargetAnsi;       // A NULL-terminated string 260 Bytes
                                        // (TargetAnsi cont'd for 57 rows)

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 520)]
        public byte[] TargetUnicode;    // An optional, NULL-terminated, Unicode string 520 Bytes
                                        // (TargetUnicode cont'd for 122 rows)
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class IconEnvironmentDataBlock : ExtraDataBlock      // An IconEnvironmentDataBlock structure (section 2.5.5).           
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 260)]
        public byte[] TargetAnsi;       // A NULL-terminated string 260 Bytes
                                        // (TargetAnsi cont'd for 57 rows)

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 520)]
        public byte[] TargetUnicode;    // An optional, NULL-terminated, Unicode string 520 Bytes
                                        // (TargetUnicode cont'd for 122 rows)
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class KnownFolderDataBlock : ExtraDataBlock   // A KnownFolderDataBlock structure (section 2.5.6).           
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public byte[] KnownFolderID;

        public UInt32 Offset;           // the location of the ItemID of the first 
    }

    //do NOT marshal this! variable size!
    public class PropertyStoreDataBlock : ExtraDataBlock // A PropertyStoreDataBlock structure (section 2.5.7).           
    {
        public UInt32 StoreSize;

        public List<PropertyStorage> Storages = new List<PropertyStorage>();

        public TypedPropertyValue GetIntValue(Guid format_id, int property_id)
        {
            PropertyStorage storage = GetStorage(format_id);
            if (storage == null)
                return null;

            foreach (SerializedIntegerValue pair in storage.Integers)
            {
                if (pair.Id == property_id)
                    return pair.Value;
            }

            return null;
        }

        public PropertyStorage GetStorage(Guid format_id)
        {
            foreach (PropertyStorage storage in Storages)
            {
                if (storage.FormatID == format_id)
                    return storage;
            }
            return null;
        }

        public TypedPropertyValue GetStringValue(Guid format_id, string property_name)
        {
            PropertyStorage storage = GetStorage(format_id);
            if (storage == null)
                return null;

            foreach (SerializedStringValue pair in storage.Strings)
            {
                if (pair.Name == property_name)
                    return pair.Value;
            }

            return null;
        }
    }

    public class PropertyStorage
    {
        public UInt32 Version;
        public Guid FormatID; //16 bytes
        public UInt32 StorageSize;

        public bool HasStrings = false; //if true, Strings is filled; otherwise Integers is filled.
        public List<SerializedStringValue> Strings = new List<SerializedStringValue>();
        public List<SerializedIntegerValue> Integers = new List<SerializedIntegerValue>();
    }


    public class SerializedStringValue
    {
        public UInt32 ValueSize; //misleading; size of whole struct, not just the value
        public UInt32 NameSize;
        public byte Reserved;
        public string Name;
        public TypedPropertyValue Value;
    }

    public class SerializedIntegerValue
    {
        public UInt32 ValueSize; //misleading; size of whole struct, not just the value
        public UInt32 Id;
        public byte Reserved;
        public TypedPropertyValue Value;
    }

    

    

    public class TypedPropertyValue
    {
        public TypedPropertyValueType Type;
        public UInt16 Padding;
        public object value;
    }

    //do NOT marshal this! variable size!
    public class ShimDataBlock : ExtraDataBlock
    {
        public string LayerName;
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class SpecialFolderDataBlock : ExtraDataBlock // A SpecialFolderDataBlock structure (section 2.5.9).           
    {
        public UInt32 SpecialFolderID;  // unsigned integer that specifies the folder integer ID.
        public UInt32 Offset;           // the first child segment of the IDList specified by SpecialFolderID.
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public class TrackerDataBlock : ExtraDataBlock        // A TrackerDataBlock structure (section 2.5.10).           
    {
        public UInt32 Length;           // This value MUST be greater than or equal to 0x0000058.
        public UInt32 Version;          // This value MUST be 0x00000000.

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public byte[] MachineID; //despite documentation, this is fixed 16 bytes length unicode string

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public byte[] Droid1;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public byte[] Droid2;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public byte[] DroidBirth1;

        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
        public byte[] DroidBirth2;
    }

    //do not marshall!
    struct VistaAndAboveIDListDataBlock     // A VistaAndAboveIDListDataBlock structure (section 2.5.11).
    {
        public LinkTargetIDList ItemIDList;
    }
}