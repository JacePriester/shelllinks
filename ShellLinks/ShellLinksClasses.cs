﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

public static partial class ShellLinks
{
    [Flags]
    public enum LinkFlags : Int32
    {
        HasLinkTargetIDList = 0x1,
        HasLinkInfo = 0x2,
        HasName = 0x4,
        HasRelativePath = 0x8,
        HasWorkingDir = 0x10,
        HasArguments = 0x20,
        HasIconLocation = 0x40,
        IsUnicode = 0x80,
        ForceNoLinkInfo = 0x100,
        HasExpString = 0x200,
        RunInSeparateProcess = 0x400,
        Unused1 = 0x800,
        HasDarwinID = 0x1000,
        RunAsUser = 0x2000,
        HasExpIcon = 0x4000,
        NoPidlAlias = 0x8000,
        Unused2 = 0x10000,
        RunWithShimLayer = 0x20000,
        ForceNoLinkTrack = 0x40000,
        EnableTargetMetadata = 0x80000,
        DisableLinkPathTracking = 0x100000,
        DisableKnownFolderTracking = 0x200000,
        DisableKnownFolderAlias = 0x400000,
        AllowLinkToLink = 0x800000,
        UnaliasOnSave = 0x1000000,
        PreferEnvironmentPath = 0x2000000,
        KeepLocalIDListForUNCTarget = 0x4000000
    }

    [Flags]
    public enum FileAttributesFlags : Int32
    {
        FILE_ATTRIBUTE_READONLY = 1,
        FILE_ATTRIBUTE_HIDDEN = 2,
        FILE_ATTRIBUTE_SYSTEM = 4,
        Reserved1 = 8,
        FILE_ATTRIBUTE_DIRECTORY = 16,
        FILE_ATTRIBUTE_ARCHIVE = 32,
        Reserved2 = 64,
        FILE_ATTRIBUTE_NORMAL = 128,
        FILE_ATTRIBUTE_TEMPORARY = 256,
        FILE_ATTRIBUTE_SPARSE_FILE = 512,
        FILE_ATTRIBUTE_REPARSE_POINT = 1024,
        FILE_ATTRIBUTE_COMPRESSED = 2048,
        FILE_ATTRIBUTE_OFFLINE = 4096,
        FILE_ATTRIBUTE_NOT_CONTENT_INDEXED = 8192,
        FILE_ATTRIBUTE_ENCRYPTED = 16384
    }

    public enum ShowCommands : Int32
    {
        SW_SHOWNORMAL = 0x1,
        SW_SHOWMAXIMIZED = 0x3,
        SW_SHOWMINNOACTIVE = 0x7
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct LinkCLSID
    {
        public byte i1, i2, i3, i4, i5, i6, i7, i8,
            i9, i10, i11, i12, i13, i14, i15, i16;
    }

    //see: https://msdn.microsoft.com/en-us/library/dd891314.aspx
    [StructLayout(LayoutKind.Explicit)]
    public struct ShellLinkHeader
    {
        [FieldOffset(0)] public int HeaderSize;
        [FieldOffset(4)] public LinkCLSID LinkCLSID;
        [FieldOffset(20)] public LinkFlags LinkFlags;
        [FieldOffset(24)] public FileAttributesFlags FileAttributes;
        [FieldOffset(28)] public System.Runtime.InteropServices.ComTypes.FILETIME CreationTime;
        [FieldOffset(36)] public System.Runtime.InteropServices.ComTypes.FILETIME AccessTime;
        [FieldOffset(44)] public System.Runtime.InteropServices.ComTypes.FILETIME WriteTime;
        [FieldOffset(52)] public Int32 FileSize;
        [FieldOffset(56)] public Int32 IconIndex;
        [FieldOffset(60)] public ShowCommands ShowCommand;
        [FieldOffset(64)] public UInt16 HotKey;
        [FieldOffset(66)] public UInt16 Reserved1;
        [FieldOffset(68)] public Int32 Reserved2;
        [FieldOffset(72)] public Int32 Reserved3;
        //total size = 76
    }

    //https://msdn.microsoft.com/en-us/library/dd891268.aspx
    public class LinkTargetIDList
    {
        public UInt16 IDListSize;
        public List<ItemID> ItemIDList;
    }

    public enum ClassTypeIndicator
    {
        Unknown,
        CLSID_ShellDesktop,
        CLSID_MyComputer,
        CLSID_ShellFSFolder,
        CLSID_NetworkRoot,
        CLSID_Internet,
        CLSID_ControlPanel,
        CLSID_ControlPanelTasks,
        CLSID_Printers,
        CLSID_CommonPlacesFolder,
        CLSID_UsersFilesFolder
    }

    //https://msdn.microsoft.com/en-us/library/dd871407.aspx
    public class ItemID
    {
        public UInt16 ItemIDSize;
        public byte[] data;

        //an attempt at parsing the data section
        public byte ClassTypeIndicatorByte;
        public byte SortIndex;
        public Guid ShellFolderIdentifier;

        public ClassTypeIndicator ClassTypeIndicator;
    }

    //https://github.com/libyal/libfwsi/blob/master/documentation/Windows%20Shell%20Item%20format.asciidoc
    static ClassTypeIndicator GetClassTypeIndicator(byte ClassTypeIndiatorByte)
    {
        byte x = ClassTypeIndiatorByte;
        if (x == 0x1e || x == 0x1f)
            return ClassTypeIndicator.CLSID_ShellDesktop;

        if (x >= 0x20 && x <= 0x2f)
            return ClassTypeIndicator.CLSID_MyComputer;

        if (x >= 0x30 && x <= 0x3f)
            return ClassTypeIndicator.CLSID_ShellFSFolder;

        if (x >= 0x40 && x <= 0x4f)
            return ClassTypeIndicator.CLSID_NetworkRoot;

        if (x == 0x61)
            return ClassTypeIndicator.CLSID_Internet;

        if (x == 0x70)
            return ClassTypeIndicator.CLSID_ControlPanel;

        if (x == 0x71)
            return ClassTypeIndicator.CLSID_ControlPanelTasks;

        if (x == 0x72)
            return ClassTypeIndicator.CLSID_Printers;

        if (x == 0x73)
            return ClassTypeIndicator.CLSID_CommonPlacesFolder;

        if (x == 0x74)
            return ClassTypeIndicator.CLSID_UsersFilesFolder;
        return ClassTypeIndicator.Unknown;
    }

    public enum DriveType : UInt32
    {
        DRIVE_UNKNOWN = 0,
        DRIVE_NO_ROOT_DIR = 1,
        DRIVE_REMOVABLE = 2,
        DRIVE_FIXED = 3,
        DRIVE_REMOTE = 4,
        DRIVE_CDROM = 5,
        DRIVE_RAMDISK = 6
    }

    public class VolumeID
    {
        public UInt32 VolumeIDSize;
        public DriveType DriveType; //uint32
        public UInt32 DriveSerialNumber;
        public UInt32 VolumeLabelOffset; //if this is 0x14, use VolumeLabelOffsetUnicode instead!!!
        public UInt32 VolumeLabelOffsetUnicode;
        public string VolumeLabel; //also called Data by the documentation.
    }

    [Flags]
    public enum LinkInfoFlags : UInt32
    {
        HasVolumeIDAndLocalBasePath = 0x1,
        HasCommonNetworkRelativeLinkAndPathSuffix = 0x2
    }

    [Flags]
    public enum CommonNetworkRelativeLinkFlags : UInt32
    {
        ValidDevice = 0x1,
        ValidNetType = 0x2
    }

    public enum NetworkProviderType : UInt32
    {
        WNNC_NET_AVID = 0x001A0000,
        WNNC_NET_DOCUSPACE = 0x001B0000,
        WNNC_NET_MANGOSOFT = 0x001C0000,
        WNNC_NET_SERNET = 0x001D0000,
        WNNC_NET_RIVERFRONT1 = 0x001E0000,
        WNNC_NET_RIVERFRONT2 = 0x001F0000,
        WNNC_NET_DECORB = 0x00200000,
        WNNC_NET_PROTSTOR = 0x00210000,
        WNNC_NET_FJ_REDIR = 0x00220000,
        WNNC_NET_DISTINCT = 0x00230000,
        WNNC_NET_TWINS = 0x00240000,
        WNNC_NET_RDR2SAMPLE = 0x00250000,
        WNNC_NET_CSC = 0x00260000,
        WNNC_NET_3IN1 = 0x00270000,
        WNNC_NET_EXTENDNET = 0x00290000,
        WNNC_NET_STAC = 0x002A0000,
        WNNC_NET_FOXBAT = 0x002B0000,
        WNNC_NET_YAHOO = 0x002C0000,
        WNNC_NET_EXIFS = 0x002D0000,
        WNNC_NET_DAV = 0x002E0000,
        WNNC_NET_KNOWARE = 0x002F0000,
        WNNC_NET_OBJECT_DIRE = 0x00300000,
        WNNC_NET_MASFAX = 0x00310000,
        WNNC_NET_HOB_NFS = 0x00320000,
        WNNC_NET_SHIVA = 0x00330000,
        WNNC_NET_IBMAL = 0x00340000,
        WNNC_NET_LOCK = 0x00350000,
        WNNC_NET_TERMSRV = 0x00360000,
        WNNC_NET_SRT = 0x00370000,
        WNNC_NET_QUINCY = 0x00380000,
        WNNC_NET_OPENAFS = 0x00390000,
        WNNC_NET_AVID1 = 0x003A0000,
        WNNC_NET_DFS = 0x003B0000,
        WNNC_NET_KWNP = 0x003C0000,
        WNNC_NET_ZENWORKS = 0x003D0000,
        WNNC_NET_DRIVEONWEB = 0x003E0000,
        WNNC_NET_VMWARE = 0x003F0000,
        WNNC_NET_RSFX = 0x00400000,
        WNNC_NET_MFILES = 0x00410000,
        WNNC_NET_MS_NFS = 0x00420000,
        WNNC_NET_GOOGLE = 0x00430000
    }

    public class CommonNetworkRelativeLink
    {
        public UInt32 CommonNetworkRelativeLinkSize;
        public CommonNetworkRelativeLinkFlags CommonNetworkRelativeLinkFlags;
        public UInt32 NetNameOffset;
        public UInt32 DeviceNameOffset;
        public NetworkProviderType NetworkProviderType;
        public UInt32 NetNameOffsetUnicode;
        public UInt32 DeviceNameOffsetUnicode;
        public string NetName;
        public string DeviceName;
        public string NetNameUnicode;
        public string DeviceNameUnicode;
    }

    public class LinkInfo
    {
        public UInt32 LinkInfoSize;
        public UInt32 LinkInfoHeaderSize;
        public LinkInfoFlags LinkInfoFlags;
        public UInt32 VolumeIDOffset;
        public UInt32 LocalBasePathOffset;
        public UInt32 CommonNetworkRelativeLinkOffset;
        public UInt32 CommonPathSuffixOffset;


        public UInt32 LocalBasePathOffsetUnicode; //optional, only present if LinkInfoHeaderSize > 0x24
        public UInt32 CommonPathSuffixOffsetUnicode; //optional, only present if LinkInfoHeaderSize > 0x24

        public VolumeID VolumeID; //optional, present if VolumeIDAndLocalBasePath flag is set in LinkInfoFlags
        public string LocalBasePath; //optional, present if VolumeIDAndLocalBasePath flag is set in LinkInfoFlags. variable length, null terminated, check for unicode.
        public CommonNetworkRelativeLink CommonNetworkRelativeLink; //optional, present if CommonNetworkRelativeLinkAndPathSuffix flag is set in LinkInfoFlags

        public string CommonPathSuffix; //variable length, null terminated, check for unicode.

        public string LocalBasePathUnicode; //optional, null terminated unicode string. present if VolumeIDAndLocalBasePath is set in flags and LinkInfoHeaderSize > 0x24.

        public string CommonPathSuffixUnicode; //optional, null terminated unicode string. present if LinkInfoHeaderSize > 0x24.
    }

    class StringData
    {
        public UInt16 CountCharacters;
        public string TheString;
    }

    [Flags]
    public enum FillAttributes : UInt16
    {
        FOREGROUND_BLUE = 0x0001,
        FOREGROUND_GREEN = 0x0002,
        FOREGROUND_RED = 0x0004,
        FOREGROUND_INTENSITY = 0x0008,
        BACKGROUND_BLUE = 0x0010,
        BACKGROUND_GREEN = 0x0020,
        BACKGROUND_RED = 0x0040,
        BACKGROUND_INTENSITY = 0x0080
    }

    public enum FontFamily : UInt32
    {
        FF_DONTCARE = 0x0000,
        FF_ROMAN = 0x0010,
        FF_SWISS = 0x0020,
        FF_MODERN = 0x0030,
        FF_SCRIPT = 0x0040,
        FF_DECORATIVE = 0x0050
    }
}