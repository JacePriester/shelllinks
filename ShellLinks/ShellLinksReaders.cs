﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

public static partial class ShellLinks
{
    static T[] SubArray<T>(this T[] data, int index, int length)
    {
        T[] result = new T[length];
        Array.Copy(data, index, result, 0, length);
        return result;
    }

    //https://github.com/libyal/libfwsi/blob/master/documentation/Windows%20Shell%20Item%20format.asciidoc
    static LinkTargetIDList ReadLinkTargetIDList(BinaryReader br)
    {
        LinkTargetIDList output = new LinkTargetIDList();

        output.IDListSize = br.ReadUInt16();

        output.ItemIDList = new List<ItemID>();
        int bytes_remaining = output.IDListSize;

        while (bytes_remaining > 0)
        {
            ItemID itemid = new ItemID();

            itemid.ItemIDSize = br.ReadUInt16();

            bytes_remaining -= 2;

            int datasize = (itemid.ItemIDSize == 0 ? 0 : itemid.ItemIDSize - 2);
            itemid.data = new byte[datasize];
            if (datasize != 0)
            {
                br.Read(itemid.data, 0, datasize);
                bytes_remaining -= datasize;

                //make an attempt at parsing it
                if (itemid.data.Length >= 18)
                {
                    itemid.ClassTypeIndicatorByte = itemid.data[0];
                    itemid.SortIndex = itemid.data[1];
                    itemid.ShellFolderIdentifier = new Guid(SubArray<byte>(itemid.data, 2, 16));

                    itemid.ClassTypeIndicator = GetClassTypeIndicator(itemid.ClassTypeIndicatorByte);
                    if (itemid.data.Length > 20)
                    {
                        //offset 20 is the 0xbeef0017 extension block
                        //not really enough known to parse it ourselves!
                    }
                }
                output.ItemIDList.Add(itemid);
            }
        }

        return output;
    }

    static string ReadNullTerminatedString(BinaryReader br, bool is_unicode)
    {
        if (is_unicode)
        {
            List<byte> allbytes = new List<byte>();

            while (true)
            {
                byte[] pair = br.ReadBytes(2);
                if (pair[0] == 0 && pair[1] == 0) //unicode null term
                    break;

                allbytes.Add(pair[0]);
                allbytes.Add(pair[1]);
            }

            if (allbytes.Count == 0)
                return "";
            
            byte[] asarray = allbytes.ToArray();
            return Encoding.Unicode.GetString(asarray, 0, asarray.Length);
        }
        else
        {
            List<byte> allbytes = new List<byte>();

            while (true)
            {
                byte single = br.ReadByte();
                if (single == 0)
                    break;

                allbytes.Add(single);
            }

            if (allbytes.Count == 0)
                return "";
            
            byte[] asarray = allbytes.ToArray();
            return Encoding.UTF8.GetString(asarray, 0, asarray.Length);
        }
    }

    static VolumeID ReadVolumeID(BinaryReader br)
    {
        long start = br.BaseStream.Position;

        VolumeID vid = new VolumeID();
        vid.VolumeIDSize = br.ReadUInt32();
        vid.DriveType = (DriveType)br.ReadUInt32();
        vid.DriveSerialNumber = br.ReadUInt32();
        vid.VolumeLabelOffset = br.ReadUInt32();

        bool is_unicode = (vid.VolumeLabelOffset == 0x14);

        if(is_unicode)
            vid.VolumeLabelOffsetUnicode = br.ReadUInt32();

        //if there is data inbetween (there shouldn't be!) we are not supposed to care.
        uint offset = is_unicode ? vid.VolumeLabelOffsetUnicode : vid.VolumeLabelOffset;
        br.BaseStream.Seek(start + offset, SeekOrigin.Begin);

        vid.VolumeLabel = ReadNullTerminatedString(br, is_unicode);

        return vid;
    }

    static CommonNetworkRelativeLink ReadCommonNetworkRelativeLink(BinaryReader br, bool unicode_strings)
    {
        long start = br.BaseStream.Position;

        CommonNetworkRelativeLink rel = new CommonNetworkRelativeLink();

        rel.CommonNetworkRelativeLinkSize = br.ReadUInt32();
        rel.CommonNetworkRelativeLinkFlags = (CommonNetworkRelativeLinkFlags)br.ReadUInt32();
        rel.NetNameOffset = br.ReadUInt32();
        rel.DeviceNameOffset = br.ReadUInt32();
        rel.NetworkProviderType = (NetworkProviderType)br.ReadUInt32();

        bool is_unicode = (rel.NetNameOffset > 0x14);

        if (is_unicode)
        {
            rel.NetNameOffsetUnicode = br.ReadUInt32();
            rel.DeviceNameOffsetUnicode = br.ReadUInt32();
        }

        br.BaseStream.Seek(start + rel.NetNameOffset, SeekOrigin.Begin);
        rel.NetName = ReadNullTerminatedString(br, unicode_strings);

        br.BaseStream.Seek(start + rel.DeviceNameOffset, SeekOrigin.Begin);
        rel.DeviceName = ReadNullTerminatedString(br, unicode_strings);

        if (is_unicode)
        {
            br.BaseStream.Seek(start + rel.NetNameOffsetUnicode, SeekOrigin.Begin);
            rel.NetNameUnicode = ReadNullTerminatedString(br, true);

            br.BaseStream.Seek(start + rel.DeviceNameOffsetUnicode, SeekOrigin.Begin);
            rel.DeviceNameUnicode = ReadNullTerminatedString(br, true);
        }

        //make sure we end at the right spot
        br.BaseStream.Seek(start + rel.CommonNetworkRelativeLinkSize, SeekOrigin.Begin);
        
        return rel;
    }

    static LinkInfo ReadLinkInfo(BinaryReader br)
    {
        long start = br.BaseStream.Position;

        LinkInfo li = new LinkInfo();

        li.LinkInfoSize = br.ReadUInt32();
        li.LinkInfoHeaderSize = br.ReadUInt32();
        li.LinkInfoFlags = (LinkInfoFlags)br.ReadUInt32();
        li.VolumeIDOffset = br.ReadUInt32();
        li.LocalBasePathOffset = br.ReadUInt32();
        li.CommonNetworkRelativeLinkOffset = br.ReadUInt32();
        li.CommonPathSuffixOffset = br.ReadUInt32();

        if (li.LinkInfoFlags.HasFlag(LinkInfoFlags.HasVolumeIDAndLocalBasePath))
            li.LocalBasePathOffsetUnicode = br.ReadUInt32();

        if (li.LinkInfoFlags.HasFlag(LinkInfoFlags.HasCommonNetworkRelativeLinkAndPathSuffix))
            li.CommonPathSuffixOffsetUnicode = br.ReadUInt32();

        br.BaseStream.Seek(start + li.VolumeIDOffset, SeekOrigin.Begin);

        if (li.LinkInfoFlags.HasFlag(LinkInfoFlags.HasVolumeIDAndLocalBasePath))
            li.VolumeID = ReadVolumeID(br);

        if (li.LinkInfoFlags.HasFlag(LinkInfoFlags.HasVolumeIDAndLocalBasePath))
        {
            br.BaseStream.Seek(start + li.LocalBasePathOffset, SeekOrigin.Begin);
            li.LocalBasePath = ReadNullTerminatedString(br, false);
        }

        if (li.LinkInfoFlags.HasFlag(LinkInfoFlags.HasCommonNetworkRelativeLinkAndPathSuffix))
        {
            br.BaseStream.Seek(start + li.CommonNetworkRelativeLinkOffset, SeekOrigin.Begin);
            li.CommonNetworkRelativeLink = ReadCommonNetworkRelativeLink(br, false);
        }

        if (li.LinkInfoFlags.HasFlag(LinkInfoFlags.HasCommonNetworkRelativeLinkAndPathSuffix))
        {
            br.BaseStream.Seek(start + li.CommonPathSuffixOffset, SeekOrigin.Begin);
            li.CommonPathSuffix = ReadNullTerminatedString(br, false);
        }

        if (li.LinkInfoFlags.HasFlag(LinkInfoFlags.HasVolumeIDAndLocalBasePath))
        {
            br.BaseStream.Seek(start + li.LocalBasePathOffsetUnicode, SeekOrigin.Begin);
            li.LocalBasePathUnicode = ReadNullTerminatedString(br, true);
        }

        if (li.LinkInfoFlags.HasFlag(LinkInfoFlags.HasCommonNetworkRelativeLinkAndPathSuffix))
        {
            br.BaseStream.Seek(start + li.CommonPathSuffixOffsetUnicode, SeekOrigin.Begin);
            li.CommonPathSuffixUnicode = ReadNullTerminatedString(br, true);
        }

        //this structure is a retarded mess so let's ensure we ended at the right spot.
        br.BaseStream.Seek(start + li.LinkInfoSize, SeekOrigin.Begin);

        return li;
    }

    static string ReadCodePageString(BinaryReader br, bool unicode_strings)
    {
        UInt32 Size = br.ReadUInt32(); //size in bytes of the Characters field
                                       //includes the null terminator, does NOT include padding.
                                       //padded to 4-byte alignment.

        //WARNING:
        //the documentation says Size is in bytes, but it appears to be in characters!!!
        //

        if (Size == 0)
            return "";

        UInt32 padding = 0;
        if (Size % 4 != 0)
            padding = 4 - Size % 4;

        int nbytes = (int)Size;
        if (unicode_strings)
            nbytes *= 2;

        string TheString = "";
        if (unicode_strings)
        {
            byte[] buf = new byte[nbytes];
            br.Read(buf, 0, nbytes);

            //subtract 2 so we don't include the null terminator
            TheString = Encoding.Unicode.GetString(buf, 0, buf.Length - 2);
        }
        else
        {
            byte[] buf = new byte[nbytes];
            br.Read(buf, 0, nbytes);

            //subtract 1 so we don't include the null terminator
            TheString = Encoding.UTF8.GetString(buf, 0, buf.Length - 1);
        }
        
        br.ReadBytes((int)padding);

        return TheString;
    }

    static StringData ReadStringData(BinaryReader br, bool unicode_strings)
    {
        StringData output = new StringData();

        output.CountCharacters = br.ReadUInt16();

        if (output.CountCharacters == 0)
        {
            output.TheString = "";
            return output;
        }

        if (unicode_strings)
        {
            int nbytes = output.CountCharacters * 2;
            byte[] buf = new byte[nbytes];
            br.Read(buf, 0, nbytes);
            output.TheString = Encoding.Unicode.GetString(buf, 0, nbytes);
        }
        else
        {
            byte[] buf = new byte[output.CountCharacters];
            br.Read(buf, 0, output.CountCharacters);
            output.TheString = Encoding.UTF8.GetString(buf, 0, output.CountCharacters);
        }

        if (output.TheString == null)
            output.TheString = "";

        return output;
    }

    static ShellLinkHeader ReadHeaderUsingPointer(byte[] data)
    {
        unsafe
        {
            fixed (byte* packet = &data[0])
            {
                return *(ShellLinkHeader*)packet;
            }
        }
    }

    static ShellLinkHeader ReadLnkHeader(BinaryReader br)
    {
        int len = Marshal.SizeOf(typeof(ShellLinkHeader));
        byte[] buf = new byte[len];
        br.Read(buf, 0, len);
        return ReadHeaderUsingPointer(buf);
    }

    static object ReadTypedPropertyValue_ReadOne(BinaryReader br, bool propset_unicode_strings, TypedPropertyValueType realtype)
    {
        if (realtype == TypedPropertyValueType.VT_EMPTY || realtype == TypedPropertyValueType.VT_NULL)
            return null;

        if (realtype == TypedPropertyValueType.VT_I2)
            return br.ReadInt16();

        if (realtype == TypedPropertyValueType.VT_I4)
            return br.ReadInt32();

        if (realtype == TypedPropertyValueType.VT_R4)
            return br.ReadSingle();

        if (realtype == TypedPropertyValueType.VT_R8)
            return br.ReadDouble();

        if (realtype == TypedPropertyValueType.VT_CY)
        {
            CURRENCY c = new CURRENCY();
            c.value = br.ReadInt64();
            return c;
        }

        if (realtype == TypedPropertyValueType.VT_DATE)
        {
            DATE d = new DATE();
            d.value = br.ReadDouble();
            return d;
        }

        if (realtype == TypedPropertyValueType.VT_BSTR)
            return ReadCodePageString(br, propset_unicode_strings);
        
        if (realtype == TypedPropertyValueType.VT_ERROR)
            return br.ReadUInt32();

        if (realtype == TypedPropertyValueType.VT_BOOL)
        {
            UInt32 i_bool = br.ReadUInt32();
            return (i_bool != 0); //either 0x0000 or 0xFFFF
        }

        if (realtype == TypedPropertyValueType.VT_DECIMAL)
            return br.ReadDecimal(); //fixme: DECIMAL struct? 16 bytes matches though so...

        if (realtype == TypedPropertyValueType.VT_I1)
        {
            sbyte value = br.ReadSByte();
            br.ReadBytes(3); //padding

            return value;
        }

        if (realtype == TypedPropertyValueType.VT_UI1)
        {
            byte value = br.ReadByte();
            br.ReadBytes(3); //padding
            return value;
        }

        if (realtype == TypedPropertyValueType.VT_UI2)
        {
            UInt16 value = br.ReadUInt16();
            br.ReadBytes(2); //padding
            return value;
        }

        if (realtype == TypedPropertyValueType.VT_UI4)
            return br.ReadUInt32();

        if (realtype == TypedPropertyValueType.VT_I8)
            return br.ReadInt64();

        if (realtype == TypedPropertyValueType.VT_UI8)
            return br.ReadUInt64();

        if (realtype == TypedPropertyValueType.VT_INT)
            return br.ReadInt32();

        if (realtype == TypedPropertyValueType.VT_UINT)
            return br.ReadUInt32();

        if (realtype == TypedPropertyValueType.VT_LPSTR)
        {
            //technically a CodePageString but it's the same as a StringData
            StringData str = ReadStringData(br, propset_unicode_strings);
            return str.TheString;
        }

        if (realtype == TypedPropertyValueType.VT_LPWSTR)
            return ReadCodePageString(br, true); //force unicode for this one
        
        if (realtype == TypedPropertyValueType.VT_FILETIME)
        {
            FILETIME ft = new FILETIME();
            ft.dwLowDateTime = br.ReadUInt32();
            ft.dwHighDateTime = br.ReadUInt32();
            return ft;
        }

        if (realtype == TypedPropertyValueType.VT_BLOB ||
            realtype == TypedPropertyValueType.VT_BLOB_OBJECT)
        {
            BLOB b = new BLOB();
            b.Size = br.ReadUInt32();
            //Size does not include padding. padded to 4-byte length.

            UInt32 padding = 0;
            if (b.Size % 4 != 0)
                padding = 4 - b.Size % 4;
            b.bytes = new byte[b.Size];
            br.Read(b.bytes, 0, b.bytes.Length);

            br.ReadBytes((int)padding);

            return b;
        }

        if (realtype == TypedPropertyValueType.VT_STREAM ||
            realtype == TypedPropertyValueType.VT_STORAGE ||
            realtype == TypedPropertyValueType.VT_STREAMED_OBJECT ||
            realtype == TypedPropertyValueType.VT_STORED_OBJECT)
        {
            //IndirectPropertyName, which is really a codepagestring
            return ReadCodePageString(br, propset_unicode_strings);
        }

        if (realtype == TypedPropertyValueType.VT_CF)
        {
            ClipboardData cd = new ClipboardData();

            cd.Size = br.ReadUInt32();
            //Size does not include padding. padded to 4-byte length.

            cd.Format = br.ReadUInt32();

            UInt32 bytes_size = cd.Size - 4;

            UInt32 padding = 0;
            if (bytes_size % 4 != 0)
                padding = 4 - bytes_size % 4;
            cd.Data = new byte[bytes_size];
            br.Read(cd.Data, 0, cd.Data.Length);

            br.ReadBytes((int)padding);

            return cd;
        }

        if (realtype == TypedPropertyValueType.VT_CLSID)
            return new Guid(br.ReadBytes(16));

        if (realtype == TypedPropertyValueType.VT_VERSIONED_STREAM)
        {
            VersionedStream vs = new VersionedStream();
            
            vs.VersionGuid = new Guid(br.ReadBytes(16));

            //indrectpropertyname
            vs.StreamName = ReadCodePageString(br, propset_unicode_strings);
            return vs;
        }

        //unrecognized!
        return null;
    }

    //https://msdn.microsoft.com/en-us/library/dd942532.aspx
    static TypedPropertyValue ReadTypedPropertyValue(BinaryReader br, bool propset_unicode_strings)
    {
        TypedPropertyValue output = new TypedPropertyValue();
        output.Type = (TypedPropertyValueType)br.ReadUInt16();
        output.Padding = br.ReadUInt16();

        //remove the only two flags that could combine with it
        TypedPropertyValueType realtype = output.Type;
        realtype &= ~TypedPropertyValueType.VT_ARRAY;
        realtype &= ~TypedPropertyValueType.VT_VECTOR;

        //if there is a header to read, read it first.
        if (output.Type.HasFlag(TypedPropertyValueType.VT_ARRAY))
        {
            List<object> list = new List<object>();

            UInt32 Type = br.ReadUInt32(); //we have this value already as realtype
            UInt32 NumDimensions = br.ReadUInt32();

            int n_items = 1;
            for (int i = 0; i < NumDimensions; i += 1)
            {
                UInt32 ArrayDimensionSize = br.ReadUInt32();
                Int32 ArrayDimensionIndexOffset = br.ReadInt32(); //dont care about this!

                n_items *= (int)ArrayDimensionSize;
            }

            for (int i = 0; i < n_items; i += 1)
                list.Add(ReadTypedPropertyValue_ReadOne(br, propset_unicode_strings, realtype));
            //fixme: we aren't preserving the array structure correctly, do we care?

            output.value = list;
        }
        else if (output.Type.HasFlag(TypedPropertyValueType.VT_VECTOR))
        {
            List<object> list = new List<object>();

            UInt32 n_items = br.ReadUInt32(); //this is the vector header

            for (int i = 0; i < n_items; i += 1)
                list.Add(ReadTypedPropertyValue_ReadOne(br, propset_unicode_strings, realtype));

            output.value = list;
        }
        else //read one
        {
            output.value = ReadTypedPropertyValue_ReadOne(br, propset_unicode_strings, realtype);
        }
        
        return output;
    }

    //returns NULL if hit the end
    static SerializedStringValue ReadPropString(BinaryReader br, bool propset_unicode_strings)
    {
        long start = br.BaseStream.Position;

        SerializedStringValue str = new SerializedStringValue();
        str.ValueSize = br.ReadUInt32();
        if (str.ValueSize == 0)
            return null;

        str.NameSize = br.ReadUInt32(); //size in bytes of Name field, including null terminator.

        str.Reserved = br.ReadByte();

        byte[] buf = new byte[str.NameSize];
        br.Read(buf, 0, buf.Length);
        str.Name = Encoding.Unicode.GetString(buf, 0, buf.Length - 2); //dont include the null terminator.

        str.Value = ReadTypedPropertyValue(br, propset_unicode_strings);

        //make sure the position is correct
        br.BaseStream.Seek(start + str.ValueSize, SeekOrigin.Begin);

        return str;
    }

    //returns NULL if hit the end
    static SerializedIntegerValue ReadPropInt(BinaryReader br, bool propset_unicode_strings)
    {
        long start = br.BaseStream.Position;

        SerializedIntegerValue number = new SerializedIntegerValue();
        number.ValueSize = br.ReadUInt32();
        if (number.ValueSize == 0)
            return null;

        number.Id = br.ReadUInt32();
        number.Reserved = br.ReadByte();
        number.Value = ReadTypedPropertyValue(br, propset_unicode_strings);

        //make sure the position is correct
        br.BaseStream.Seek(start + number.ValueSize, SeekOrigin.Begin);

        return number;
    }

    static List<ExtraData> ReadExtraDataBlocks(BinaryReader br, bool unicode_strings)
    {
        List<ExtraData> blocks = new List<ExtraData>();

        while (true)
        {
            ExtraData block = new ExtraData();
            block.BlockSize = br.ReadUInt32();

            if (block.BlockSize < 4) //this is the terminal marker, we read them all!
                break; //and yes we have to check for <4 instead of just zero for some reason

            block.BlockSignature = (DataBlockSignatures)br.ReadUInt32();

            long previous_location = br.BaseStream.Position;
            block.RawData = new byte[(int)block.BlockSize - 8];
            br.Read(block.RawData, 0, block.RawData.Length);

            GCHandle handle = GCHandle.Alloc(block.RawData, GCHandleType.Pinned);

            if (block.BlockSignature == DataBlockSignatures.ConsoleDataBlock)
                block.Data = Marshal.PtrToStructure<ConsoleDataBlock>(handle.AddrOfPinnedObject());
            else if (block.BlockSignature == DataBlockSignatures.ConsoleFEDataBlock)
                block.Data = Marshal.PtrToStructure<ConsoleFEDataBlock>(handle.AddrOfPinnedObject());
            else if (block.BlockSignature == DataBlockSignatures.DarwinDataBlock)
                block.Data = Marshal.PtrToStructure<DarwinDataBlock>(handle.AddrOfPinnedObject());
            else if (block.BlockSignature == DataBlockSignatures.EnvironmentVariableDataBlock)
                block.Data = Marshal.PtrToStructure<EnvironmentVariableDataBlock>(handle.AddrOfPinnedObject());
            else if (block.BlockSignature == DataBlockSignatures.IconEnvironmentDataBlock)
                block.Data = Marshal.PtrToStructure<IconEnvironmentDataBlock>(handle.AddrOfPinnedObject());
            else if (block.BlockSignature == DataBlockSignatures.KnownFolderDataBlock)
                block.Data = Marshal.PtrToStructure<KnownFolderDataBlock>(handle.AddrOfPinnedObject());
            else if (block.BlockSignature == DataBlockSignatures.PropertyStoreDataBlock)
            {
                br.BaseStream.Seek(previous_location, SeekOrigin.Begin);
                PropertyStoreDataBlock store = new PropertyStoreDataBlock();

                //the documentation indicates we need to read another value, but it isn't very clear about it.
                //testing shows we already have it as the block size.
                store.StoreSize = block.BlockSize;// br.ReadUInt32();

                while (true) //loop and reaad each PropertyStorage until its StorageSize hits zero
                {
                    PropertyStorage storage = new PropertyStorage();
                    storage.StorageSize = br.ReadUInt32();
                    if (storage.StorageSize == 0) //terminal
                        break;

                    storage.Version = br.ReadUInt32();
                    if (storage.Version != 0x53505331)
                        throw new Exception("Version error in property storage");

                    storage.FormatID = new Guid(br.ReadBytes(16));

                    //this is the magic GUID that indicates strings; all others are integers
                    if (storage.FormatID == Guid.Parse("D5CDD505-2E9C-101B-9397-08002B2CF9AE"))
                    {
                        storage.HasStrings = true;

                        while (true)
                        {
                            SerializedStringValue str = ReadPropString(br, unicode_strings);
                            if (str == null)
                                break;

                            storage.Strings.Add(str);
                        }
                    }
                    else
                    {
                        storage.HasStrings = false;

                        while (true)
                        {
                            SerializedIntegerValue number = ReadPropInt(br, unicode_strings);
                            if (number == null)
                                break;

                            storage.Integers.Add(number);
                        }
                    }

                    store.Storages.Add(storage);
                }
            
                block.Data = store;
            }
            else if (block.BlockSignature == DataBlockSignatures.ShimDataBlock)
            {
                br.BaseStream.Seek(previous_location, SeekOrigin.Begin);
                ShimDataBlock data = new ShimDataBlock();
                byte[] buf = new byte[block.BlockSize - 8];

                br.Read(buf, 0, buf.Length);
                data.LayerName = Encoding.Unicode.GetString(buf, 0, buf.Length);

                block.Data = data;
            }
            else if (block.BlockSignature == DataBlockSignatures.SpecialFolderDataBlock)
                block.Data = Marshal.PtrToStructure<SpecialFolderDataBlock>(handle.AddrOfPinnedObject());
            else if (block.BlockSignature == DataBlockSignatures.TrackerDataBlock)
                block.Data = Marshal.PtrToStructure<TrackerDataBlock>(handle.AddrOfPinnedObject());
            else if (block.BlockSignature == DataBlockSignatures.VistaAndAboveIDListDataBlock)
            {
                br.BaseStream.Seek(previous_location, SeekOrigin.Begin);
                VistaAndAboveIDListDataBlock data = new VistaAndAboveIDListDataBlock();
                data.ItemIDList = ReadLinkTargetIDList(br);
            }

            blocks.Add(block);
        }

        return blocks;
    }
}
