﻿using System;

public static partial class ShellLinks
{
    public class CURRENCY
    {
        public Int64 value;
    }

    public class DATE
    {
        public double value;
    }

    public class FILETIME
    {
        public UInt32 dwLowDateTime;
        public UInt32 dwHighDateTime;
    }

    public class BLOB
    {
        public UInt32 Size; //length of bytes array
        public byte[] bytes;
    }

    public class ClipboardData
    {
        public UInt32 Size; //size of Format and Data, not including padding
        public UInt32 Format;
        public byte[] Data; //length will be size - 4
    }

    public class VersionedStream
    {
        public Guid VersionGuid; //16 bytes
        public string StreamName;
    }

    [Flags] //careful: the only flags are VT_ARRAY and VT_VECTOR combined with 1 other value
    public enum TypedPropertyValueType : UInt16
    {
        VT_EMPTY = 0x0000,
        VT_NULL = 0x0001,
        VT_I2 = 0x0002,
        VT_I4 = 0x0003,
        VT_R4 = 0x0004,
        VT_R8 = 0x0005,
        VT_CY = 0x0006,
        VT_DATE = 0x0007,
        VT_BSTR = 0x0008,
        VT_ERROR = 0x000A,
        VT_BOOL = 0x000B,
        VT_DECIMAL = 0x000E,
        VT_I1 = 0x0010,
        VT_UI1 = 0x0011,
        VT_UI2 = 0x0012,
        VT_UI4 = 0x0013,
        VT_I8 = 0x0014,
        VT_UI8 = 0x0015,
        VT_INT = 0x0016,
        VT_UINT = 0x0017,
        VT_LPSTR = 0x001E,
        VT_LPWSTR = 0x001F,
        VT_FILETIME = 0x0040,
        VT_BLOB = 0x0041,
        VT_STREAM = 0x0042,
        VT_STORAGE = 0x0043,
        VT_STREAMED_OBJECT = 0x0044,
        VT_STORED_OBJECT = 0x0045,
        VT_BLOB_OBJECT = 0x0046,
        VT_CF = 0x0047,
        VT_CLSID = 0x0048,
        VT_VERSIONED_STREAM = 0x0049,
        VT_VECTOR = 0x1000,
        VT_ARRAY = 0x2000
        //VT_VECTOR | VT_I2 = 0x1002,
        //VT_VECTOR | VT_I4 = 0x1003,
        //VT_VECTOR | VT_R4 = 0x1004,
        //VT_VECTOR | VT_R8 = 0x1005,
        //VT_VECTOR | VT_CY = 0x1006,
        //VT_VECTOR | VT_DATE = 0x1007,
        //VT_VECTOR | VT_BSTR = 0x1008,
        //VT_VECTOR | VT_ERROR = 0x100A,
        //VT_VECTOR | VT_BOOL = 0x100B,
        //VT_VECTOR | VT_VARIANT = 0x100C,
        //VT_VECTOR | VT_I1 = 0x1010,
        //VT_VECTOR | VT_UI1 = 0x1011,
        //VT_VECTOR | VT_UI2 = 0x1012,
        //VT_VECTOR | VT_UI4 = 0x1013,
        //VT_VECTOR | VT_I8 = 0x1014,
        //VT_VECTOR | VT_UI8 = 0x1015,
        //VT_VECTOR | VT_LPSTR = 0x101E,
        //VT_VECTOR | VT_LPWSTR = 0x101F,
        //VT_VECTOR | VT_FILETIME = 0x1040,
        //VT_VECTOR | VT_CF = 0x1047,
        //VT_VECTOR | VT_CLSID = 0x1048,
        //VT_ARRAY | VT_I2 = 0x2002,
        //VT_ARRAY | VT_I4 = 0x2003,
        //VT_ARRAY | VT_R4 = 0x2004,
        //VT_ARRAY | VT_R8 = 0x2005,
        //VT_ARRAY | VT_CY = 0x2006,
        //VT_ARRAY | VT_DATE = 0x2007,
        //VT_ARRAY | VT_BSTR = 0x2008,
        //VT_ARRAY | VT_ERROR = 0x200A,
        //VT_ARRAY | VT_BOOL = 0x200B,
        //VT_ARRAY | VT_VARIANT = 0x200C,
        //VT_ARRAY | VT_DECIMAL = 0x200E,
        //VT_ARRAY | VT_I1 = 0x2010,
        //VT_ARRAY | VT_UI1 = 0x2011,
        //VT_ARRAY | VT_UI2 = 0x2012,
        //VT_ARRAY | VT_UI4 = 0x2013,
        //VT_ARRAY | VT_INT = 0x2016,
        //VT_ARRAY | VT_UINT = 0x2017,
    }
}