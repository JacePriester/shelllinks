﻿using System;
using System.Collections.Generic;
using System.IO;

public static partial class ShellLinks
{
    public class LnkData
    {
        public string lnkpath; //same as whatever is passed to ParseLnk()

        public ShellLinkHeader ShellLinkHeader;
        public LinkTargetIDList LinkTargetIDList;
        public LinkInfo LinkInfo;
        public string comment = "";
        public string relative_path = "";
        public string working_dir = "";
        public string arguments = "";
        public string icon_location = "";
        public List<ExtraData> extras = new List<ExtraData>();

        public string GetShortcutTarget()
        {
            string test = GetLocalBasePath();
            if (test != "")
                return Environment.ExpandEnvironmentVariables(test);

            return Environment.ExpandEnvironmentVariables(ResolveRelativePath());
        }

        //not valid unless IsLikelyShellItem is true
        public Guid GetShellItemPartialGuid()
        {
            if (!IsLikelyShellItem())
                return Guid.Empty;

            //we can assume this exists based on the check above.
            ItemID first = LinkTargetIDList.ItemIDList[0];
            return first.ShellFolderIdentifier;
        }

        public bool IsLikelyShellItem()
        {
            if (!ShellLinkHeader.LinkFlags.HasFlag(LinkFlags.HasLinkTargetIDList))
                return false;
            if (LinkTargetIDList == null)
                return false;
            if (LinkTargetIDList.ItemIDList.Count == 0)
                return false;
            ItemID first = LinkTargetIDList.ItemIDList[0];
            if (first.ClassTypeIndicator == ClassTypeIndicator.Unknown)
                return false;
            
            return true;
        }

        string GetLocalBasePath()
        {
            if (LinkInfo != null)
            {
                if (LinkInfo.LocalBasePathUnicode != "")
                    return LinkInfo.LocalBasePathUnicode;
                return LinkInfo.LocalBasePath;
            }
            return "";
        }

        string ResolveRelativePath()
        {
            if (relative_path == "")
                return "";

            string location = Path.GetDirectoryName(lnkpath);
            string complete = Path.Combine(location, relative_path);
            string resolved_path = Path.GetFullPath((new Uri(complete)).LocalPath);
            return resolved_path;
        }

        public string GetParsingPath()
        {
            //see if we can get the parsing path, which is useful for special items like Recycle Bin
            PropertyStoreDataBlock store = GetExtraDataBlock<PropertyStoreDataBlock>();
            if (store != null)
            {
                TypedPropertyValue res = store.GetIntValue(new Guid("28636aa6-953d-11d2-b5d6-00c04fd918d0"), 30);
                if (res != null && res.value is string)
                {
                    return (string)res.value;
                }
            }

            return "";
        }

        public T GetExtraDataBlock<T>() where T : ExtraDataBlock
        {
            foreach (ExtraData ed in extras)
            {
                if (ed.Data is T)
                    return (T)ed.Data;
            }
            return null;
        }
    }
    
    public static LnkData ParseLnk(string lnkpath)
    {
        using (FileStream fs = new FileStream(lnkpath, FileMode.Open, FileAccess.Read))
        {
            LnkData data = new LnkData();
            data.lnkpath = lnkpath;
            
            BinaryReader br = new BinaryReader(fs);
            try
            {
                
                data.ShellLinkHeader = ReadLnkHeader(br);

                bool unicode_strings = data.ShellLinkHeader.LinkFlags.HasFlag(LinkFlags.IsUnicode);
                    
                if (data.ShellLinkHeader.LinkFlags.HasFlag(LinkFlags.HasLinkTargetIDList))
                    data.LinkTargetIDList = ReadLinkTargetIDList(br);
                if (data.ShellLinkHeader.LinkFlags.HasFlag(LinkFlags.HasLinkInfo))
                    data.LinkInfo = ReadLinkInfo(br);
                if (data.ShellLinkHeader.LinkFlags.HasFlag(LinkFlags.HasName))
                    data.comment = ReadStringData(br, unicode_strings).TheString;
                
                if (data.ShellLinkHeader.LinkFlags.HasFlag(LinkFlags.HasRelativePath))
                    data.relative_path = ReadStringData(br, unicode_strings).TheString;
                if (data.ShellLinkHeader.LinkFlags.HasFlag(LinkFlags.HasWorkingDir))
                    data.working_dir = ReadStringData(br, unicode_strings).TheString;
                if (data.ShellLinkHeader.LinkFlags.HasFlag(LinkFlags.HasArguments))
                    data.arguments = ReadStringData(br, unicode_strings).TheString;
                if (data.ShellLinkHeader.LinkFlags.HasFlag(LinkFlags.HasIconLocation))
                {
                    string raw_location = Environment.ExpandEnvironmentVariables(ReadStringData(br, unicode_strings).TheString);
                    data.icon_location = Path.GetFullPath(raw_location);
                }
                
                data.extras = ReadExtraDataBlocks(br, unicode_strings);
                
                return data;
            }
            catch
            {
            }
        }

        return null;
    }
}